<?php /* Smarty version Smarty-3.1.21, created on 2018-06-03 22:43:47
         compiled from "/Applications/XAMPP/xamppfiles/htdocs/E/market-place/design/backend/templates/addons/product_variations/hooks/products/update_tools_list.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17238484245b1444f3b06418-56448991%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2a5588abbd982d2918bd8cea7a5191a9b1b54129' => 
    array (
      0 => '/Applications/XAMPP/xamppfiles/htdocs/E/market-place/design/backend/templates/addons/product_variations/hooks/products/update_tools_list.post.tpl',
      1 => 1525682414,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '17238484245b1444f3b06418-56448991',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product_data' => 0,
    'id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5b1444f3b13ad3_16879198',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5b1444f3b13ad3_16879198')) {function content_5b1444f3b13ad3_16879198($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('product_variations.confirm_convert_to_configurable_product','product_variations.convert_to_configurable_product','product_variations.variations'));
?>
<?php if ($_smarty_tpl->tpl_vars['product_data']->value['product_type']===constant("\Tygh\Addons\ProductVariations\Product\Manager::PRODUCT_TYPE_SIMPLE")) {?>
    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'data'=>array("data-ca-confirm-text"=>$_smarty_tpl->__("product_variations.confirm_convert_to_configurable_product")),'text'=>$_smarty_tpl->__("product_variations.convert_to_configurable_product"),'href'=>"product_variations.convert?product_id=".((string)$_smarty_tpl->tpl_vars['id']->value),'class'=>"cm-confirm",'method'=>"POST"));?>
</li>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['product_data']->value['product_type']===constant("\Tygh\Addons\ProductVariations\Product\Manager::PRODUCT_TYPE_CONFIGURABLE")) {?>
    <li><?php ob_start();
echo htmlspecialchars(constant("\Tygh\Addons\ProductVariations\Product\Manager::PRODUCT_TYPE_VARIATION"), ENT_QUOTES, 'UTF-8');
$_tmp8=ob_get_clean();?><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("product_variations.variations"),'href'=>"products.manage?parent_product_id=".((string)$_smarty_tpl->tpl_vars['id']->value)."&product_type=".$_tmp8));?>
</li>
<?php }?>
<?php }} ?>
